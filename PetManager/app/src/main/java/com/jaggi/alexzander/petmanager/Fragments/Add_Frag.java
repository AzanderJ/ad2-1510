package com.jaggi.alexzander.petmanager.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.jaggi.alexzander.petmanager.Data.DataManager;
import com.jaggi.alexzander.petmanager.Data.GooglePlaceAdapter;
import com.jaggi.alexzander.petmanager.Data.PetObject;
import com.jaggi.alexzander.petmanager.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * PetManager
 * Created by Alexzander on 11/3/15.
 */
public class Add_Frag extends Fragment implements AdapterView.OnItemSelectedListener, GoogleApiClient.OnConnectionFailedListener,GoogleApiClient.ConnectionCallbacks,AlertDialog.OnClickListener {

    private static final int REQUEST_TAKE_PICTURE = 0x01001;
    private static final int GOOGLE_API_ID = 0x098;
    private static final int SELECT_IMG = 0x09919;


    private EditText mNameText;
    private EditText mIDText;
    private EditText mLicenseText;
    private EditText mSpeciesText;
    private EditText mFoodText;
    private AutoCompleteTextView mVetLocText;
    private EditText mVetNumText;
    private EditText mMedText;
    private EditText mOtherText;
    private EditText mAgeText;
    private ImageView imageView;
    private Uri mImageUri = null;
    private String mGender;
    private final String[] genders = {"Male","Female"};
    private boolean isNew;
    private int mIndex;
    private String addressString;
    private GoogleApiClient googleApiClient;
    private GooglePlaceAdapter googlePlaceAdapter;
    private double mLat;
    private double mLng;



    public Add_Frag() {
    }
    public static Add_Frag newInstance(){
        Bundle args = new Bundle();
        Add_Frag fragment = new Add_Frag();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_frag,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(getString(R.string.addNewTitle));

        //view items
        imageView = (ImageView)view.findViewById(R.id.addImageView);
        mNameText = (EditText)view.findViewById(R.id.nameEditText);
        mIDText = (EditText)view.findViewById(R.id.idEditText);
        mLicenseText = (EditText)view.findViewById(R.id.licenseTextEdit);
        mSpeciesText = (EditText)view.findViewById(R.id.speciesEditText);
        mFoodText = (EditText)view.findViewById(R.id.foodEditText);
        mVetLocText = (AutoCompleteTextView)view.findViewById(R.id.vetLocEditText);
        mVetNumText = (EditText)view.findViewById(R.id.vetNumEditText);
        mMedText = (EditText)view.findViewById(R.id.medEditText);
        mOtherText = (EditText)view.findViewById(R.id.otherEditText);
        mAgeText = (EditText)view.findViewById(R.id.ageEditText);
        Spinner mSpinner = (Spinner) view.findViewById(R.id.spinner);
        ArrayAdapter<String> adapter =new ArrayAdapter<>(getActivity(),R.layout.spinner_layout,genders);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        mSpinner.setOnItemSelectedListener(this);

        //get starting intent to determine if new or edited
        Intent intent = getActivity().getIntent();
        isNew = intent.getBooleanExtra("new", false);
        //if edited load pet details into views
        if (!isNew){
            getActivity().setTitle(getString(R.string.editTitleString));
            PetObject petObject = (PetObject) intent.getSerializableExtra("object");
            mIndex = intent.getIntExtra("index",0);
            if (Objects.equals(petObject.getmImage(), "noImage")){
                imageView.setImageResource(R.drawable.no_image_available);
                mImageUri = null;
            }else {

                imageView.setImageBitmap(DataManager.getImage(petObject.getmImage(), 500, 500));
                mImageUri = Uri.parse(petObject.getmImage());
            }
            mNameText.setText(petObject.getmName());
            mIDText.setText(petObject.getmID());
            mLicenseText.setText(petObject.getmLicense());
            mSpeciesText.setText(petObject.getmSpecies());
            mSpeciesText.setEnabled(false);
            mFoodText.setText(petObject.getmFood());
            mVetLocText.setText(petObject.getmVetLocDesc());
            mVetNumText.setText(petObject.getmVetNum());
            mMedText.setText(petObject.getmMedication());
            mOtherText.setText(petObject.getmOther());
            mAgeText.setText(petObject.getmAge());
            mLat = petObject.getmLat();
            mLng = petObject.getmLng();
            addressString= petObject.getmVetLoc();
            if (Objects.equals(petObject.getmGender(), "Female")){
                mSpinner.setSelection(1);
            }else {
                mSpinner.setSelection(0);
            }
        }
        setupGooglePredictions();
    }

    private void setupGooglePredictions() {
        //google places address search
        googleApiClient = new GoogleApiClient.Builder(getActivity()).addApi(Places.GEO_DATA_API).enableAutoManage((FragmentActivity) getActivity(), GOOGLE_API_ID, this).addConnectionCallbacks(this).build();
        mVetLocText.setThreshold(3);
        mVetLocText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GooglePlaceAdapter.PlaceAutoComplete item = googlePlaceAdapter.getItem(position);
                String placeId = String.valueOf(item.placeId);
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(googleApiClient, placeId);
                placeResult.setResultCallback(resultsCallback);
            }
        });
        googlePlaceAdapter = new GooglePlaceAdapter(getActivity(), android.R.layout.simple_list_item_1,null);
        mVetLocText.setAdapter(googlePlaceAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.save_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            //actionbar save
            case R.id.action_save:
                String mImageString;
                if (mImageUri == null){
                    mImageString = "noImage";
                }else {
                    mImageString = mImageUri.toString();
                }
                if (Objects.equals(mSpeciesText.getText().toString(), "")){
                    mSpeciesText.setText(R.string.unkown);
                }
                PetObject petObject = new PetObject(
                        mGender,
                        mNameText.getText().toString(),
                        mIDText.getText().toString(),
                        mLicenseText.getText().toString(),
                        mSpeciesText.getText().toString(),
                        mFoodText.getText().toString(),
                        addressString,
                        mVetNumText.getText().toString(),
                        mImageString,
                        mMedText.getText().toString(),
                        mAgeText.getText().toString(),
                        mOtherText.getText().toString(),
                        mVetLocText.getText().toString(),
                        mLat,
                        mLng
                );
                if (isNew){
                    DataManager.setNewPetObject(getActivity(), petObject);
                    Intent backIntent = new Intent();
                    getActivity().setResult(Activity.RESULT_OK, backIntent);
                }else {
                    DataManager.saveEdited(getActivity(),petObject,mIndex);
                }

                getActivity().finish();
                break;

            //actionbar camera
            case R.id.action_camera:
                getIMG();
        }
        return super.onOptionsItemSelected(item);
    }

    private Uri getImageUri() {

        @SuppressLint("SimpleDateFormat") String imageName = new SimpleDateFormat("MMddyyyy_HHmmss").format(new Date(System.currentTimeMillis()));

        File imageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File appDir = new File(imageDir, "Pet Manager");
        if (!appDir.exists()){
            //noinspection ResultOfMethodCallIgnored
            appDir.mkdir();
        }

        File image = new File(appDir, imageName + ".jpg");


        return Uri.fromFile(image);
    }

    private void addImageToGallery(Uri imageUri) {
        Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        scanIntent.setData(imageUri);
        getActivity().sendBroadcast(scanIntent);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //spinner view selection
        mGender = genders[position];
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(Bundle bundle) {
        googlePlaceAdapter.setGoogleApiClient(googleApiClient);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onStop() {
        super.onStop();
        if (googleApiClient.isConnected()){
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //camera activity finished
        if(requestCode == REQUEST_TAKE_PICTURE && resultCode != Activity.RESULT_CANCELED ) {
            if(mImageUri != null) {
                imageView.setImageBitmap(DataManager.getImage(mImageUri.toString(),500,500));
                addImageToGallery(mImageUri);
            } else {
                imageView.setImageBitmap((Bitmap)data.getParcelableExtra("data"));
            }
        }
        else if (requestCode == SELECT_IMG){
            if (data !=null) {
                Uri imgSelected = data.getData();
                String[] imgPathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getActivity().getContentResolver().query(imgSelected, imgPathColumn, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                    int index = cursor.getColumnIndex(imgPathColumn[0]);
                    String imgPath = cursor.getString(index);
                    mImageUri = Uri.parse(imgPath);
                    imageView.setImageBitmap(DataManager.getImage(imgPath, 500, 500));
                    cursor.close();

                }

            }
        }
    }

    private ResultCallback<PlaceBuffer> resultsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                places.getStatus();
                return;
            }
            //get and set selected address information
            Place place = places.get(0);
            CharSequence address = place.getAddress();
            LatLng latLng = place.getLatLng();
            mLat = latLng.latitude;
            mLng = latLng.longitude;
            addressString = String.valueOf(address);
            if (Objects.equals(mVetNumText.getText().toString(), "")) {
                mVetNumText.setText(place.getPhoneNumber());
            }
            places.release();
        }
    };

    private void getIMG(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Image");
        builder.setPositiveButton("Camera", this);
        builder.setNeutralButton("Cancel", this);
        builder.setNegativeButton("Gallery",this);
        builder.setIcon(R.drawable.animals2);
        builder.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which){
            case -1: // camera
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                mImageUri = getImageUri();
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
                startActivityForResult(intent, REQUEST_TAKE_PICTURE);
                break;
            case -2: //gallery
                Intent intent1 = new Intent();
                intent1.setType("image/*");
                intent1.setAction(Intent.ACTION_PICK);
                intent1.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(intent1, SELECT_IMG);
                break;

            case -3: // cancel

                break;
        }
    }
}
