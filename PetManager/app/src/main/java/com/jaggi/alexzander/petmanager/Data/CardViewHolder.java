package com.jaggi.alexzander.petmanager.Data;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jaggi.alexzander.petmanager.R;

/**
 * PetManager
 * Created by Alexzander on 11/3/15.
 */
public class CardViewHolder extends RecyclerView.ViewHolder {

    protected ImageView mImageView;
    protected TextView mNameText;
    protected CardView card;

    public CardViewHolder(View itemView) {
        super(itemView);
        mNameText = (TextView)itemView.findViewById(R.id.card_info_text);
        mImageView = (ImageView)itemView.findViewById(R.id.cardImageView);
        card = (CardView)itemView;


    }
}
