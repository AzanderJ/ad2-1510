package com.jaggi.alexzander.petmanager.Fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.jaggi.alexzander.petmanager.Data.DataManager;
import com.jaggi.alexzander.petmanager.Data.PetObject;
import com.jaggi.alexzander.petmanager.R;
import com.jaggi.alexzander.petmanager.SecondActivity;

import java.io.File;
import java.util.Objects;

/**
 * PetManager
 * Created by Alexzander on 11/9/15.
 */
public class View_Frag extends Fragment implements View.OnClickListener, AlertDialog.OnClickListener {

    private int mIndex;
    private PetObject petObject;
    private CheckBox checkBox;


    public View_Frag() {
    }
    public static View_Frag newInstance(){
        Bundle args = new Bundle();
        View_Frag fragment = new View_Frag();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_frag, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        //get pet object
        Intent intent = getActivity().getIntent();
        petObject = (PetObject) intent.getSerializableExtra("petObject");
        mIndex = intent.getIntExtra("index",0);

        //start map fragment
        Map_Frag map_frag = Map_Frag.newInstance();
        getChildFragmentManager().beginTransaction().replace(R.id.mapV,map_frag,"mapFrag").commit();

        //get views
        ImageView mImageViewer = (ImageView) view.findViewById(R.id.imageViewer);
        TextView mName = (TextView) view.findViewById(R.id.nameV);
        TextView mSpecies = (TextView) view.findViewById(R.id.SpeciesV);
        TextView mBorn = (TextView) view.findViewById(R.id.bDayV);
        TextView mGender = (TextView) view.findViewById(R.id.genderV);
        TextView mID = (TextView) view.findViewById(R.id.idV);
        TextView mLicense = (TextView) view.findViewById(R.id.licenseV);
        TextView mFood = (TextView) view.findViewById(R.id.foodV);
        TextView mMeds = (TextView) view.findViewById(R.id.medV);
        TextView mOther = (TextView) view.findViewById(R.id.otherV);
        TextView mVetPhone = (TextView) view.findViewById(R.id.vetPhoneV);
        TextView mVetaddress = (TextView) view.findViewById(R.id.vetAddressV);
        Button mDeleteButton = (Button)view.findViewById(R.id.deleteButton);
        mDeleteButton.setOnClickListener(this);

        //determine what image to use
        if (Objects.equals(petObject.getmImage(), "noImage")){
            mImageViewer.setImageResource(R.drawable.no_image_available);
        }else {
            mImageViewer.setImageBitmap(DataManager.getImage(petObject.getmImage(), 500, 500));
        }

        //set label text
        mName.setText(petObject.getmName());
        mSpecies.setText(petObject.getmSpecies());
        mBorn.setText(petObject.getmAge());
        mGender.setText(petObject.getmGender());
        mID.setText(petObject.getmID());
        mLicense.setText(petObject.getmLicense());
        mFood.setText(petObject.getmFood());
        mMeds.setText(petObject.getmMedication());
        mOther.setText(petObject.getmOther());
        mVetPhone.setText(petObject.getmVetNum());
        mVetaddress.setText(petObject.getmVetLoc());

    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.view_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            //actionbar edit
            case R.id.action_edit:
                Intent intent = new Intent(getActivity(), SecondActivity.class);
                intent.putExtra("new",false);
                intent.putExtra("object", petObject);
                intent.putExtra("index", mIndex);
                startActivity(intent);
                getActivity().finish();
                break;

            //actionbar email
            case R.id.action_share:
                Intent intentShare = new Intent(Intent.ACTION_SEND);
                intentShare.setType("text/html");
                intentShare.putExtra(Intent.EXTRA_SUBJECT, "Details for my Pet " + petObject.getmName());
                intentShare.putExtra(Intent.EXTRA_STREAM, Uri.parse(petObject.getmImage()));
                intentShare.putExtra(Intent.EXTRA_TEXT,
                        this.getString(R.string.emName)+petObject.getmName()+
                                "\n"+this.getString(R.string.emSpecies)+ petObject.getmSpecies()+
                                "\n"+this.getString(R.string.emBorn)+petObject.getmAge()+
                                "\n"+this.getString(R.string.emGender)+petObject.getmGender()+
                                "\n"+this.getString(R.string.emID)+petObject.getmID()+
                                "\n"+this.getString(R.string.emLicense)+petObject.getmLicense()+
                                "\n"+this.getString(R.string.emFood)+petObject.getmFood()+
                                "\n"+this.getString(R.string.emMeds)+petObject.getmMedication()+
                                "\n"+this.getString(R.string.emOther)+petObject.getmOther()+
                                "\n"+this.getString(R.string.emVetPhone)+petObject.getmVetNum()+
                                "\n"+this.getString(R.string.emVetAddress)+petObject.getmVetLoc());
                startActivity(Intent.createChooser(intentShare, "Email"));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.deleteButton:
                alertUser();
                break;
        }
    }
    private void alertUser(){
        //Alert for pet deletion

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.delete_alert_layout,null);
        builder.setView(view);
        ImageView imageView = (ImageView)view.findViewById(R.id.alertImage);
        imageView.setImageBitmap(DataManager.getImage(petObject.getmImage(), 300, 300));
        checkBox = (CheckBox)view.findViewById(R.id.alertCheckBox);
        if (Objects.equals(petObject.getmImage(), "noImage")){
            ViewGroup viewGroup = (ViewGroup)checkBox.getParent();
            viewGroup.removeView(checkBox);
        }
        builder.setTitle("DELETE");
        builder.setMessage(this.getString(R.string.alertQuestion) + petObject.getmName() + this.getString(R.string.questionMark));
        builder.setNegativeButton("Cancel", this);
        builder.setPositiveButton("OK", this);
        builder.setIcon(R.drawable.animals2);
        builder.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        //cancel or confirm pet deletion

        switch (which){
            case -1:
                if (checkBox.isChecked()){
                    Uri uri = Uri.parse(petObject.getmImage());
                    File file = new File(uri.getPath());
                    if (file.exists()) {
                        //noinspection ResultOfMethodCallIgnored
                        file.delete();
                        //refresh gallery
                        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,uri);
                        getActivity().sendBroadcast(intent);
                    }
                }
                DataManager.deletePetObject(getActivity(), petObject, mIndex);
                getActivity().finish();

                break;
            case -2: //cancel

                break;
        }
    }
}
