package com.jaggi.alexzander.petmanager;

import android.gesture.Gesture;
import android.gesture.GestureOverlayView;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.jaggi.alexzander.petmanager.Fragments.View_Frag;

/**
 * PetManager
 * Created by Alexzander on 11/9/15.
 */
public class ViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimaryDark));

        View_Frag view_frag = View_Frag.newInstance();
        getFragmentManager().beginTransaction().replace(R.id.viewMainContainer,view_frag).commit();



    }
}
