package com.jaggi.alexzander.petmanager.Data;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * PetManager
 * Created by Alexzander on 11/2/15.
 */
public class DataManager {

    //save new pet
    public static void setNewPetObject(Context context, PetObject theObject){
        HashMap<String,ArrayList<PetObject>> objectStore = getPetObjects(context);
        ArrayList<PetObject> arrayList = (ArrayList)getPetArrayForKey(context,theObject.getmSpecies());
        try {
            FileOutputStream fileOutputStream = context.openFileOutput("saved.dat", Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            arrayList.add(theObject);
            objectStore.put(theObject.getmSpecies(),arrayList);
            objectOutputStream.writeObject(objectStore);
            fileOutputStream.close();
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //save edited pet
    public static void saveEdited(Context context, PetObject theObject, int _index){
        HashMap<String,ArrayList<PetObject>> objectStore = getPetObjects(context);
        ArrayList<PetObject> arrayList = (ArrayList)getPetArrayForKey(context,theObject.getmSpecies());
        try {
            FileOutputStream fileOutputStream = context.openFileOutput("saved.dat", Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            arrayList.set(_index, theObject);
            objectStore.put(theObject.getmSpecies(),arrayList);
            objectOutputStream.writeObject(objectStore);
            fileOutputStream.close();
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //delete pet
    public static void deletePetObject(Context context, PetObject petObject, int _index){
        HashMap<String,ArrayList<PetObject>> objectStore = getPetObjects(context);
        ArrayList<PetObject> arrayList = (ArrayList)getPetArrayForKey(context,petObject.getmSpecies());
        arrayList.remove(_index);

        if (arrayList.isEmpty()){
            SharedPreferences sharedPreferences = context.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove("index");
            editor.putString("index", "Pet Manager");
            editor.apply();
        }
        objectStore.put(petObject.getmSpecies(),arrayList);
        try {
            FileOutputStream fileOutputStream = context.openFileOutput("saved.dat", Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(objectStore);
            fileOutputStream.close();
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //get array for species
    public static ArrayList<PetObject> getPetArrayForKey(Context context, String key){
        HashMap<String,ArrayList<PetObject>> storeObjects;
        ArrayList<PetObject> arrayList = null;

        try {
            FileInputStream fileInputStream = context.openFileInput("saved.dat");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            storeObjects = (HashMap<String,ArrayList<PetObject>>)objectInputStream.readObject();
            fileInputStream.close();
            objectInputStream.close();
            arrayList = storeObjects.get(key);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (arrayList == null){
            arrayList = new ArrayList<>();
        }

        return arrayList;
    }
    //get keys for species arrays
    public static ArrayList<String> getKeys(Context context){
        ArrayList<String> arrayList = new ArrayList<>();
        HashMap<String,ArrayList<PetObject>> objectStore = getPetObjects(context);
        for (String key : objectStore.keySet() ){
            ArrayList<PetObject> list = objectStore.get(key);
            if (!list.isEmpty()){
                arrayList.add(key);

            }
        }
        return arrayList;
    }
    //get image with size
    public static Bitmap getImage(String _imageString,int _width, int _height){
        Uri picURI = Uri.parse(_imageString);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(picURI.getPath(),options);
        options.inSampleSize = getSampleSize(options,_width,_height);
        options.inJustDecodeBounds =false;
        return BitmapFactory.decodeFile(picURI.getPath(),options);
    }
    //get hash map
    private static HashMap<String,ArrayList<PetObject>> getPetObjects(Context context){
        HashMap<String,ArrayList<PetObject>> storeObjects = null;

        try {
            FileInputStream fileInputStream = context.openFileInput("saved.dat");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            storeObjects = (HashMap<String,ArrayList<PetObject>>)objectInputStream.readObject();
            fileInputStream.close();
            objectInputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (storeObjects == null){
            storeObjects = new HashMap<>();
        }

        return storeObjects;
    }
    //determine image sample size
    private static int getSampleSize(BitmapFactory.Options options, int _width, int _height){
        final int height = options.outHeight;
        final int width = options.outWidth;
        int sample = 1;
        if (height>_height || width > _width){
            final int hRatio = Math.round((float)height/(float)_height);
            final int wRatio = Math.round((float)width/(float)_width);
            sample = hRatio < wRatio ? hRatio:wRatio;
        }
        return sample;
    }


}
