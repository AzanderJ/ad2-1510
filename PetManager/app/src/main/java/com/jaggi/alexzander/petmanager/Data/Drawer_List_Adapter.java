package com.jaggi.alexzander.petmanager.Data;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jaggi.alexzander.petmanager.R;

import java.util.ArrayList;

/**
 * PetManager
 * Created by Alexzander on 11/4/15.
 */
public class Drawer_List_Adapter extends BaseAdapter {
    private ArrayList<String> objects;
    private Context context;

    public Drawer_List_Adapter(ArrayList<String> objects, Context context) {
        this.objects = objects;
        this.context = context;

    }

    public void dataSetChanged(Context context) {
        objects = DataManager.getKeys(context);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.drawer_list_layout, null);
        }

        if (objects != null){

            TextView textView = (TextView)convertView.findViewById(R.id.listText);
            textView.setText(objects.get(position));
        }
        return convertView;
    }
}
