package com.jaggi.alexzander.petmanager.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.jaggi.alexzander.petmanager.Data.Card_Adapter;
import com.jaggi.alexzander.petmanager.Data.DataManager;
import com.jaggi.alexzander.petmanager.R;
import com.jaggi.alexzander.petmanager.SecondActivity;

/**
 * PetManager
 * Created by Alexzander on 11/4/15.
 */
public class Main_Frag extends Fragment {

    private Card_Adapter cardAdapter;
    private MainFragInterface mainFragInterface;

    public Main_Frag() {
    }

    public static Main_Frag newInstance(){
        Bundle args = new Bundle();
        Main_Frag fragment = new Main_Frag();
        fragment.setArguments(args);
        return fragment;
    }

    public interface MainFragInterface{
        void refreshMenu();
        String getSpecies();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.main_frag,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //set cardview with orientation
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerList);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(linearLayoutManager);
        }else {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),2);
            gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(gridLayoutManager);
        }
        mainFragInterface = (MainFragInterface)getActivity();
        cardAdapter = new Card_Adapter(DataManager.getPetArrayForKey(getActivity(), mainFragInterface.getSpecies()));
        recyclerView.setAdapter(cardAdapter);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.main_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_new:
                Intent intent = new Intent(getActivity(), SecondActivity.class);
                intent.putExtra("new", true);
                startActivityForResult(intent, 0x01);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mainFragInterface.refreshMenu();
        cardAdapter.dataChanged(getActivity(),mainFragInterface.getSpecies());
    }

    @Override
    public void onResume() {
        super.onResume();
        mainFragInterface.refreshMenu();
        cardAdapter.dataChanged(getActivity(),mainFragInterface.getSpecies());
    }
}
