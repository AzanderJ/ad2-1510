package com.jaggi.alexzander.petmanager.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jaggi.alexzander.petmanager.Data.PetObject;

/**
 * PetManager
 * Created by Alexzander on 11/9/15.
 */
public class Map_Frag extends MapFragment {


    private GoogleMap googleMap;


    public Map_Frag() {
    }

    public static Map_Frag newInstance(){
        Bundle args = new Bundle();
        Map_Frag fragment = new Map_Frag();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //start map and set vet location pin
        initMap();
        setVetLoc();
    }

    private void initMap() {
        googleMap = getMap();
        googleMap.setMyLocationEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        setVetLoc();
    }

    private void setVetLoc(){
        //set pin for vet location
        Intent intent = getActivity().getIntent();
        PetObject petObject = (PetObject) intent.getSerializableExtra("petObject");
        LatLng latLng = new LatLng(petObject.getmLat(),petObject.getmLng());
        MarkerOptions marker = new MarkerOptions();
        marker.position(latLng);
        marker.title(petObject.getmVetLocDesc());
        marker.infoWindowAnchor(marker.getAnchorU(), marker.getAnchorV() - 0.8f);
        googleMap.addMarker(marker);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));

    }
}
