package com.jaggi.alexzander.petmanager;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jaggi.alexzander.petmanager.Fragments.Add_Frag;

/**
 * PetManager
 * Created by Alexzander on 11/2/15.
 */
public class SecondActivity extends AppCompatActivity  {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimaryDark));
        Add_Frag add_frag = Add_Frag.newInstance();
        getFragmentManager().beginTransaction().replace(R.id.addFragHolder,add_frag,"add_frag").commit();


    }


}
