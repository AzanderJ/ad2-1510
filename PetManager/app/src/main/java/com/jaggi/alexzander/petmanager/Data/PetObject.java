package com.jaggi.alexzander.petmanager.Data;

import java.io.Serializable;

/**
 * PetManager
 * Created by Alexzander on 11/2/15.
 */
public class PetObject implements Serializable {

    private static final int serialVersionUID = 10001;

    private String mGender;
    private String mName;
    private String mID;
    private String mLicense;
    private String mSpecies;
    private String mFood;
    private String mVetLoc;
    private String mVetNum;
    private String mImage;
    private String mMedication;
    private String mAge;
    private String mOther;
    private String mVetLocDesc;
    private double mLat;
    private double mLng;

    public static int getSerialVersionUID() {
        return serialVersionUID;
    }

    public PetObject() {

    }
    //pet object creator
    public PetObject(String _gender, String _name, String _id, String _license, String _species, String _food, String _vetLocation, String _vetNumber, String _image, String _medication, String _age, String _other, String _vetLocDesc, double _lat, double _lng){
        mGender = _gender;
        mName = _name;
        mID = _id;
        mLicense = _license;
        mSpecies = _species;
        mFood = _food;
        mVetLoc = _vetLocation;
        mVetNum = _vetNumber;
        mImage = _image;
        mMedication = _medication;
        mAge = _age;
        mOther = _other;
        mVetLocDesc = _vetLocDesc;
        mLat = _lat;
        mLng = _lng;
    }

    //getters
    public String getmName() {
        return mName;
    }

    public String getmID() {
        return mID;
    }

    public String getmLicense() {
        return mLicense;
    }

    public String getmSpecies() {
        return mSpecies;
    }

    public String getmFood() {
        return mFood;
    }

    public String getmVetLoc() {
        return mVetLoc;
    }

    public String getmVetNum() {
        return mVetNum;
    }

    public String getmImage() {
        return mImage;
    }

    public String getmMedication() {
        return mMedication;
    }

    public String getmOther() {
        return mOther;
    }

    public String getmGender() {
        return mGender;
    }

    public String getmAge() {
        return mAge;
    }

    public String getmVetLocDesc() {
        return mVetLocDesc;
    }

    public double getmLat() {
        return mLat;
    }

    public double getmLng() {
        return mLng;
    }

    @Override
    public String toString() {
        return "PetObject{" +
                "mGender='" + mGender + '\'' +
                ", mName='" + mName + '\'' +
                ", mID='" + mID + '\'' +
                ", mLicense='" + mLicense + '\'' +
                ", mSpecies='" + mSpecies + '\'' +
                ", mFood='" + mFood + '\'' +
                ", mVetLoc='" + mVetLoc + '\'' +
                ", mVetNum='" + mVetNum + '\'' +
                ", mImage='" + mImage + '\'' +
                ", mMedication='" + mMedication + '\'' +
                ", mAge='" + mAge + '\'' +
                ", mOther='" + mOther + '\'' +
                ", mVetLocDesc='" + mVetLocDesc + '\'' +
                ", mLat=" + mLat +
                ", mLng=" + mLng +
                '}';
    }
}
