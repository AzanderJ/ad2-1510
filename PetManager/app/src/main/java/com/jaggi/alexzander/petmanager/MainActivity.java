package com.jaggi.alexzander.petmanager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.jaggi.alexzander.petmanager.Data.Drawer_List_Adapter;
import com.jaggi.alexzander.petmanager.Fragments.Drawer_Frag;
import com.jaggi.alexzander.petmanager.Fragments.Main_Frag;

/**
 * PetManager
 * Created by Alexzander on 11/2/15.
 */

public class MainActivity extends AppCompatActivity implements Main_Frag.MainFragInterface, Drawer_Frag.DrawerFragInterface {

    private Drawer_List_Adapter drawer_list_adapter;
    private String speciesIndex;
    private SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //shared prefs for managing the app title based on current view. loads last viewed species.
        sharedPreferences = getSharedPreferences("Preferences", MODE_PRIVATE);
        speciesIndex = sharedPreferences.getString("index", "");
        this.setTitle(speciesIndex);

        //load fragments and layouts
        Drawer_Frag drawer_frag = Drawer_Frag.newInstance();
        getFragmentManager().beginTransaction().replace(R.id.menuListContainer,drawer_frag).commit();

        Main_Frag main_frag = Main_Frag.newInstance();
        getFragmentManager().beginTransaction().replace(R.id.mainFragContainer,main_frag).commit();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void refreshMenu() {
        drawer_list_adapter.dataSetChanged(this);
    }

    @Override
    public String getSpecies() {
        return speciesIndex;
    }

    @Override
    public void setAdapter(Drawer_List_Adapter adapter) {
        drawer_list_adapter = adapter;
    }

    @Override
    public void setSpecies(String species) {
        speciesIndex = species;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("index",species);
        editor.apply();
        this.setTitle(species);

    }

    @Override
    public void menuClose() {
        onBackPressed();
    }

    @Override
    public void onActivityReenter(int resultCode, Intent data) {
        super.onActivityReenter(resultCode, data);
        String titleS = sharedPreferences.getString("index","Pet Manager");
        this.setTitle(titleS);
    }
}
