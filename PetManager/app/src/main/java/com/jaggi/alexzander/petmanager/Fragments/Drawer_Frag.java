package com.jaggi.alexzander.petmanager.Fragments;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jaggi.alexzander.petmanager.Data.DataManager;
import com.jaggi.alexzander.petmanager.Data.Drawer_List_Adapter;
import com.jaggi.alexzander.petmanager.R;

import java.util.ArrayList;

/**
 * PetManager
 * Created by Alexzander on 11/4/15.
 */
public class Drawer_Frag extends Fragment implements ListView.OnItemClickListener {

    private ListView listView;
    private ArrayList menuTitles;
    private DrawerFragInterface drawerFragInterface;



    public interface DrawerFragInterface{
        void setAdapter(Drawer_List_Adapter adapter);
        void setSpecies(String species);
        void menuClose();
    }

    public static Drawer_Frag newInstance(){
        Bundle args = new Bundle();
        Drawer_Frag fragment = new Drawer_Frag();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.drawer_frag,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        drawerFragInterface = (DrawerFragInterface)getActivity();
        listView = (ListView)view.findViewById(R.id.mainMenuList);
        listView.setOnItemClickListener(this);
        menuTitles = DataManager.getKeys(getActivity());
        Drawer_List_Adapter drawer_list_adapter = new Drawer_List_Adapter(menuTitles, getActivity());
        listView.setAdapter(drawer_list_adapter);
        drawerFragInterface.setAdapter(drawer_list_adapter);
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        drawerFragInterface.menuClose();
        menuTitles = DataManager.getKeys(getActivity());
        drawerFragInterface.setSpecies(menuTitles.get(position).toString());
        Main_Frag main_frag = Main_Frag.newInstance();
        getFragmentManager().beginTransaction().replace(R.id.mainFragContainer,main_frag).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        menuTitles = DataManager.getKeys(getActivity());
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("Preferences", getActivity().MODE_PRIVATE);
        String speciesIndex = sharedPreferences.getString("index", "Pet Manager");
        drawerFragInterface.setSpecies(speciesIndex);
    }
}
