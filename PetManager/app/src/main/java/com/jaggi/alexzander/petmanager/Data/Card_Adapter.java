package com.jaggi.alexzander.petmanager.Data;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jaggi.alexzander.petmanager.R;
import com.jaggi.alexzander.petmanager.ViewActivity;

import java.util.ArrayList;
import java.util.Objects;

/**
 * PetManager
 * Created by Alexzander on 11/3/15.
 */
public class Card_Adapter extends RecyclerView.Adapter<CardViewHolder> {

    private ArrayList<PetObject> arrayList;
    int cardImageWidth = 400;
    int cardImageHeight = 400;

    public Card_Adapter(ArrayList<PetObject> arrayList) {
        this.arrayList = arrayList;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);
        return new CardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, final int position) {
        final PetObject petObject = arrayList.get(position);

        if (Objects.equals(petObject.getmImage(), "noImage")){
            holder.mImageView.setImageResource(R.drawable.no_image_available);
        }else {
            holder.mImageView.setImageBitmap(DataManager.getImage(petObject.getmImage(),cardImageWidth,cardImageHeight));
        }
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ViewActivity.class);
                intent.putExtra("petObject",petObject);
                intent.putExtra("index", position);
                v.getContext().startActivity(intent);
            }
        });
        holder.mNameText.setText(petObject.getmName());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public void dataChanged(Context context, String key){
        arrayList = DataManager.getPetArrayForKey(context,key);
        this.notifyDataSetChanged();
    }


}
